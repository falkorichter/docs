# user.toml
This configuration is located under `/etc/bffh`. 

Here the users, there Password, roles and cardkeys are stored. 

Roles are defined in the bffh.dhall configuration. The [Docker-compose Repository](https://gitlab.com/fabinfra/fabaccess/dockercompose) has a good [example](https://gitlab.com/fabinfra/fabaccess/dockercompose/-/blob/main/config/bffh/users.toml)

```exapmle of users.toml
[Admin1]
roles = ["Admin"]
passwd = "secret"
noot = "noot!"
cardkey = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"

[Admin2]
roles = ["Admin"]
passwd = "secret"
noot = "noot!"
cardkey = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"

[ManagerA1]
roles = ["ManageA", "UseA", "ReadA", "DiscloseA"]
passwd = "secret"
noot = "noot!"
cardkey = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"

[ManagerA2]
roles = ["ManageA", "UseA", "ReadA", "DiscloseA"]
passwd = "secret"
noot = "noot!"
cardkey = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"

[ManagerB1]
roles = ["ManageB", "UseB", "ReadB", "DiscloseB"]
passwd = "secret"
noot = "noot!"
cardkey = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"

[ManagerB2]
roles = ["ManageB", "UseB", "ReadB", "DiscloseB"]
passwd = "secret"
noot = "noot!"
cardkey = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"

[ManagerC1]
roles = ["ManageC", "UseC", "ReadC", "DiscloseC"]
passwd = "secret"
noot = "noot!"
cardkey = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
```