# Run Server with Docker

Docker Image can not run on armv6 (Raspberry Pi 1 or Raspberry Pi Zero)

1. Install Docker

    On Raspberry Pi: 
    [https://phoenixnap.com/kb/docker-on-raspberry-pi](https://phoenixnap.com/kb/docker-on-raspberry-pi)
2. Install Docker-Compose

    On Raspberry Pi: 
    [https://dev.to/elalemanyo/how-to-install-docker-and-docker-compose-on-raspberry-pi-1mo](https://dev.to/elalemanyo/how-to-install-docker-and-docker-compose-on-raspberry-pi-1mo)
4. Get Docker-Compose Files `git clone https://gitlab.com/fabinfra/fabaccess/dockercompose.git fabaccess-server`
    
    The Dockerfile is in the root directory of the main repo
    docker-compose.yml is available in a seperate [git repo](https://gitlab.com/fabinfra/fabaccess/dockercompose)
4. Edit config files in `config` folder to taste
5. Start Server with `docker-compose up -d`

To make it eaysier to apply youre changes in your config and keep the dockercompose uptodate, you should "fork" this respository.

Get Server Logs: `docker-compose logs`
