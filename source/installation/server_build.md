# Build Server from Source

E.g. in a proxmox- or lxc-container.

## Install Dependencies
### Ubuntu / Debian
1. `sudo apt update && sudo apt upgrade`
1. `sudo apt install curl && curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`
1. `sudo apt install libgsasl7-dev libssl-dev build-essential`
1. `sudo apt install git cmake clang capnproto`

### Arch Linux
1. `sudo pacman -Syu`
1. `sudo pacman -S make cmake clang gsasl`
1. `sudo pacman -S git rust capnproto`

### CentOS
1. `sudo yum update`
1. `sudo yum install curl && curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`
1. `sudo yum install epel-release && sudo yum install capnproto`
1. `sudo yum install https://packages.endpointdev.com/rhel/7/os/x86_64/endpoint-repo.x86_64.rpm && sudo yum install git`
1. `sudo yum install centos-release-scl && yum install llvm-toolset-7 && scl enable llvm-toolset-7 bash` (Change bash to youre shell)
1. `sudo yum install gcc-c++ libgsasl-devel openssl-devel cmake`

### FreeBSD
TODO
<!--- 1. `sudo pkg update -f`
1. `sudo pkg install curl && curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`
1. `sudo pkg install git` -->

## Build BFFH
Start new terminal - Rustup will not update path while install
1. `git clone https://gitlab.com/fabinfra/fabaccess/bffh.git --recursive`
1. `cd bffh`
1. (Optional) `git checkout development && git submodule update --remote` (Change `development` to wanted branch)
1. `cargo build --release`
